CC=gcc

all: 	bltl_parse.tab.o bltl_lex.yy.o tracever.o automaton.o 	
	$(CC) -Wall -o tracever *.o -O3  
debug:
	bison --language=c -W -v -t -d bltl_parse.y
	flex  -Cem -p -v --header-file=bltl_lex.yy.h -o bltl_lex.yy.c bltl_lex.l
	$(CC) -g -o tracever *.c -DYYDEBUG -DDVERBOSE
verbose:
#following line needs bison > 3.0
#bison --language=c -W -d -f bltl_parse.y
	bison --language=c -W -d bltl_parse.y
	flex -Cem -v --header-file=bltl_lex.yy.h -o bltl_lex.yy.c bltl_lex.l
	$(CC) -Wall -o tracever *.c -O3 -DDVERBOSE
graph:  
	bison --language=c -d -g bltl_parse.y
	dot -Tps bltl_parse.dot -o graph.pdf
clean:
	rm -f *.o *.dot bltl_lex.yy.* bltl_parse.tab.* *.pdf bltl_parse.output tracever *~

bltl_parse.tab.o: bltl_parse.y
#following line needs bison > 3.0
#bison --language=c -W -d -f bltl_parse.y
	bison --language=c -d bltl_parse.y
	$(CC) -Wall -c bltl_parse.tab.c -O3

bltl_lex.yy.o:  bltl_lex.l
	flex -Cem --header-file=bltl_lex.yy.h -o bltl_lex.yy.c bltl_lex.l
	$(CC) -Wall -c bltl_lex.yy.c -O3

tracever.o: tracever.c
	$(CC) -Wall -c tracever.c -O3

automaton.o: automaton.c automaton.h
	$(CC) -Wall -c automaton.c -O3
	

