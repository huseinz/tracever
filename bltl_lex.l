%{
#include "bltl_parse.tab.h"
#include <limits.h>
%}

%option noyywrap
%option nounput
%option noinput

%%

[ \t,]			{ }
#.*\n			{ }
"U"		      	{ 					return UNTIL; 		}
"G"		      	{ 					return GLOBAL; 		}
"F"		      	{ 					return FUTURE; 		}
[-]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)? { 	

			if(strlen(yytext) >= 100){
				fprintf(stderr, "Error: number exceeds 100 digits\n");
				exit(2);
			}
			yylval.fval = strtod(yytext, NULL);	
			return REAL;		
			} 
[a-zA-Z]([a-z \tA-Z0-9_-]*[a-zA-Z0-9])?	{ 
			
			if(strlen(yytext) >= 100){
				fprintf(stderr, "Error: identifier name exceeds 100 characters\n");
				exit(2);
			}
			yylval.sval = strdup(yytext); 	
			return PARAM;		
			}
":"			{ 					return *yytext;		}
"->"		      	{   					return IMP;		}
"<"		      	{ yylval.sval = strdup(yytext); 	return COMP; 		}
">"		      	{ yylval.sval = strdup(yytext); 	return COMP; 		}
">="		      	{ yylval.sval = strdup(yytext); 	return COMP; 		}
"<="		      	{ yylval.sval = strdup(yytext); 	return COMP; 		}
"=="	 		{ yylval.sval = strdup(yytext); 	return COMP; 		}
"!="			{ yylval.sval = strdup(yytext);		return COMP;		}
"+"		      	{   					return ADD_TOK;		}
"-"		      	{   					return SUB_TOK;		}
"*"		      	{   					return MUL_TOK;		}
"/"		      	{   					return DIV_TOK;		}
"||"		    	{					return OR;		} 
"&&"	      		{   					return AND;		}
"!"		      	{   					return NOT;		}
"("		      	{   					return *yytext;		}
")"		      	{   					return *yytext;		}
";"			{					return SEMICOL;		}
\n 	      	      	{ } 
.		      	{ printf("Warning: unreconized token '%s'\n", yytext); }

%%
