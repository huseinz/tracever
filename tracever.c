/*
	Trace Verifier

	Author: Zubir Husein

	Based on the research of Bernd Finkbeiner
	and Henny Sipma in the paper titled:
	'Checking Finite Traces Using Alternating Automata'

	July 2015
*/

#include "automaton.h"
#include "bltl_lex.yy.h"
#include "bltl_parse.tab.h"

#define DEFAULT_MAX_PARAMS 60
#define DEFAULT_TRACELENGTH 5000
#define FLAG_COUNT  3

char usage[] = "Usage: %s [options] tracefile (-s specification | -f specfile)\n";
enum {VERBOSE, CMDSPEC, SPECFILE};
bool flags[FLAG_COUNT];

extern int yyparse();
extern YYSTYPE yylval;
extern int sym_lookup(const char* str);
extern int sym_index;
extern char* sym_table[];

int main(int argc, char* argv[]) {
	
	int i, j, num_params, opt, yylex_retval;
	int max_params = DEFAULT_MAX_PARAMS;
	int tracelength = DEFAULT_TRACELENGTH;
	FILE* tracefile = stdin, *specfile = NULL;
	char* specs = NULL;	
	//stores the location of trace table param names in symbol table
	int sym_table_indices[DEFAULT_MAX_PARAMS]; 

	//parse arguments
	while((opt = getopt(argc, argv, "vs:f:l:p:")) != -1){		
		switch (opt){
		char* endptr;
		case 'v':
			flags[VERBOSE] = true;
			break;
		case 's':
			specs = optarg;
			flags[CMDSPEC] = true;
			break;
		case 'f':
			specfile = fopen(optarg, "r");
			if(!specfile){
				perror(optarg);
				exit(2);
			}
			flags[SPECFILE] = true;
			break;
		case ':':
			fprintf(stderr, "-%c requires argument\n", optopt);
			exit(2);
		case 'l':
			errno = 0;
			endptr = (char*)1;
			tracelength = strtol(optarg, &endptr, 10);
			if (*endptr != '\0' || errno == ERANGE){
				fprintf(stderr, "invalid argument to -l: '%s'\n", optarg);
				exit(2);
			}
			break;
		case 'p':
			errno = 0;
			endptr = (char*)1;
			max_params = strtol(optarg, &endptr, 10);
			if (*endptr != '\0' || errno == ERANGE){
				fprintf(stderr, "invalid argument to -p: '%s'\n", optarg);
				exit(2);
			}
			break;
		default:
			fprintf(stderr, usage, argv[0]);
			exit(2);
		}
	}
	
	//read specification and trace
	if(flags[SPECFILE]){
		//read specfile into buffer
		size_t len = 0;
		ssize_t charsread = getdelim( &specs, &len, '\0', specfile);
		if( charsread == -1){
			perror("specfile");
			exit(2);
		}
	}
	else if(!flags[CMDSPEC]){
		fprintf(stderr, "No specifications given\n");
		fprintf(stderr, usage, argv[0]);
		exit(2);
	}

	if(optind < argc){
		tracefile = fopen(argv[optind], "r");
		if(!tracefile) {
			perror(argv[optind]);
			exit(2);
		}
	}

	//parse specifications
	yy_switch_to_buffer(yy_scan_string(specs));

	if(yyparse())
		exit(2);

	yypop_buffer_state();
	
	//read parameter names, find their index in the symbol table,
	//and put them in sym_table_indices
	yy_switch_to_buffer(yy_create_buffer(tracefile, YY_BUF_SIZE));

	char** paramnames = malloc(sizeof(char*) * DEFAULT_MAX_PARAMS);

	yylex_retval = yylex();
	for(num_params = 0; yylex_retval == PARAM && num_params < max_params; num_params++){
		paramnames[num_params] = strdup(yylval.sval);
		sym_table_indices[num_params] = sym_lookup(yylval.sval);
		free(yylval.sval);
		yylex_retval = yylex();
	}

	//check if all identifiers used in specification exist in table
	for(i = 1; i < sym_index ; i++){
		for(j = 0; j < num_params; j++){
			if(strcmp(paramnames[j], sym_table[i]) == 0)
				break;
		}
		if(j == num_params){
			fprintf(stderr, "Error: identifier '%s' in specification does not exist in trace table\n", sym_table[i]);
			exit(2); 
		}
	}

	//allocate trace table
	trace_vals = malloc(sizeof(double*) * tracelength);
	if(!trace_vals){
		perror("!");
		exit(2);
	}
	for(i = 0; i < tracelength; i++){
		trace_vals[i] = malloc(sizeof(double) * (num_params + 1));
		if(!trace_vals[i]){
			perror("!");
			exit(2);
		}
	}
	
	//read trace table values
	for(i = 0; yylex_retval && i < tracelength; i++){
		for(j = 0; j < num_params && yylex_retval == REAL; j++){
			trace_vals[i][sym_table_indices[j]] = yylval.fval;
			yylex_retval = yylex();
		}
	}

	//set n_max to number of positions
	n_max = i;

	//free unneeded stuff
	fclose(tracefile);
	yypop_buffer_state();
	yylex_destroy();

	if(flags[SPECFILE]){
		fclose(specfile);
		free(specs);
	}
	for(i = 1; i < sym_index; i++)
		free(sym_table[i]);
	for(i = 0; i < num_params; i++)
		free(paramnames[i]);
	free(paramnames);
	
	//finally, run DFS
	bool DFS_retval = DFS(final_automaton, 0, INT_MAX); 

	if(flags[0]){
		printf("Input length:   %-ld\n", n_max);
		printf("DFS calls made: %-ld\n\n", DFS_calls_made);
		printf("Automaton returns ");
		puts( DFS_retval ? "true" : "false" );
	}

	//free automaton and trace table
	delete_automaton(final_automaton);

	for(i = 0; i < tracelength; i++){
		free(trace_vals[i]);
	}
	free(trace_vals);

	//return result
	return DFS_retval;
}
