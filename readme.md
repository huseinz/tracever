##trace verifier

####Description

Verifies an output trace against a bounded linear temporal logic specification.

####Synopsis

./tracever [OPTION]... (-s SPECIFICATIONS | -f SPECFILE) TRACEFILE

####Options

```
	-v
		Verbose output, execution is silent otherwise. 

	-f SPECFILE
		SPECFILE is path to a file containing the BLTL specifications, separated by semicolons.

	-s SPECIFICATIONS
		SPECIFICATIONS should be a string containing the BLTL specifications, separated by semicolons.
		This option will be ignored if -f is used.

	-l n
		Specifies the maximum length of trace buffer, defaults to 5000.
		This option should be used if the trace length can exceed the default maximum. 

	-p n
		Specifies the maximum number of parameters, defaults to 60.
		This option should be used if the number of parameters exceeds the default maximum. 

	TRACEFILE
		A path to a file containing the trace data.
		If no file is given, tracever will read from standard input.

```
####Return Status

This program returns 1 if the trace satisfied the specification, and 0 if it did not. It returns 2 if an error occurred. 
 
####Trace Syntax 

The trace file must begin with a comma-separated list of parameter names in the order that they appear
within each record of the trace. Parameter names can contain spaces. A comma or whitespace delimited 
table of parameter values then follows this list.

Example:

```
x, y
-1.2 2.22
-1.3 2.31
-1.3 2.63
 0.9 2.62
-0.7 2.33
-0.4 2.11
-0.2 2.31
-0.1 2.32
 0.3 2.31
 0.2 2.32
 0.2 2.34
 0.3 3.45
```

Lines beginning with '#' will be treated as comments and ignored. 


####Specification Syntax

Parameter names can contain spaces. 

Lines beginning with '#' will be treated as comments and ignored.

State operators:

`!`	      : NOT    
`&&`	  : AND    
`||`      : OR     
`->`      : IMPLIES       

Unary LTL operators:

`G:N`     : **G**lobally    
`F:N`     : **F**uture    

Binary LTL operators:

`U:N`     : **U**ntil     

where N is the bound ( 0 indicates infinity ) 

Numerical Comparison Operators:

`<`, `>`, `<=`, `>=`, `==`, `!=`     

Arithmetic Operators:

`+`, `-`, `*`, `/`


Use `(` and `)` to make statements unambiguous.

Multiple specifications can be given by separating them with a semicolon. This is equivalent to ANDing them. 

####Examples 

Example trace file
```
#G:1 ((y + x) < 80)) && (F:6 (x > 0))
x, y
-1.2 2.22
-1.3 2.31
-1.3 2.63
0.9 2.62
-0.7 2.33
-0.4 2.11
-0.2 2.31
-0.1 2.32
 0.3 2.31
 0.2 2.32
 0.2 2.34
 0.3 3.45

```
Example invocation
```
$ ./tracever -v trace1.csv -s 'G:1 ((y + x) < 80)) && (F:6 (x > 0))'
Input length:   12
DFS calls made: 3

Automaton returns true

```


Addition example trace files can be found in the 'examples' directory.

####Compiling

Requires flex, bison, gcc

Simply run `make`

Additional targets:
```
clean
verbose
debug
```


